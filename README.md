# 维克日记
![logo](icon.png)

## 体验极佳的Markdown日记软件

维克日记是一款设计优雅的日记软件，支持Markdown语法输入和实时预览。

![screenshot](image.png)

## 软件特性

- 一目了然的用户界面，简单免配置、开箱即用
- 支持Markdown语法输入，内容原地实时预览
- 日记支持表格可视化编辑、本地图片插入/粘贴
- 支持输入密码登录，日记隐私内容更有保障
- 支持按目录批量操作日记（如：删除等）
- 数据纯本地保存，无惧云端第三方安全风险
- 支持Windows、Mac、Linux多个系统平台

## 安装方法

### Windows

目前为Windows用户提供了NSIS安装包(setup.exe)、ZIP压缩包(.zip)和便携式可执行文件(.exe)三种格式，分别可以在release页面下载对应的文件。

### Mac

Mac用户可以使用DMG安装包(.dmg)和ZIP压缩包两种格式皆可，支持x64/arm64架构，同样位于release页面下载。

### Linux

这里为Linux用户提供了三种格式的下载文件，分别是通用的APPImage格式(.AppImage)、用于Debian系(如：Ubuntu)的deb包和Redhat系(如：CentOS)的rpm包，支持x64/arm64架构，可在release页面下载。

## 备用下载地址

> 由于部分格式安装包超过了gitee上传文件大小限制，推荐通过以下途径下载：

### 百度网盘

https://pan.baidu.com/s/1M1OeMXaWnTr8f0_oXtR08Q?pwd=c2rd 提取码: c2rd

### 城通网盘

https://url43.ctfile.com/d/3173743-57977955-91e7e2?p=3701 （访问密码：3701）